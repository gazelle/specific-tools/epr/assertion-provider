
# EPR Assertion Provider MockUp

## Overview

The EPR Assertion Provider MockUp is a SoapUI webservice (mock) that provides SAML Assertion when requested.
The Assertion Provider is based on a Gazelle Java library __saml-generator__ that need to be deployed on the running SoapUI.

* default_wsdl_url: http://ehealthsuisse.ihe-europe.net:8090/STS?wsdl
* default_path: /STS
* default_port: 8090
* default_mock_name: STSBindingMockService
* default_mock_path: /opt/simulators/epr-assertion-provider-mockup
* default_soapui_path: /usr/local/SmartBear/SoapUI-5.3.0/
* default_soapui_mock_log: /var/log/soapui/epr-assertion-provider.log
* default_init.d: /etc/init.d/mock-eCH-XUAGenerator
* default_keystore_path: /opt/gazelle/cert/jboss.jks

## Get project sources

```bash
git clone https://gitlab.inria.fr/gazelle/specific-tools/epr/assertion-provider.git $ASSERTION_PROVIDER_PROJECT_DIR
```

## Build project

```bash
cd $ASSERTION_PROVIDER_PROJECT_DIR
mvn clean package
```

## Deploy locally the mock

### Install SoapUI

[https://www.soapui.org/](https://www.soapui.org/)

Tested with SoapUI 5.3.0 and 5.5.0.

### Deploy EPR SAML Generator

Copy the jar into the external SoapUI intallation dir

```shell
cp saml-generator/target/epr-saml-generator-X.X.X-jar-with-dependencies.jar $SOAPUI_INSTALL_DIR/bin/ext/.
```

### Deploy the mock messages recorder

Get the mock-recorder jar into the external SoapUI intallation dir

```shell
cd $SOAPUI_INSTALL_DIR/bin/ext/.
wget https://gazelle.ihe.net/nexus/service/local/repositories/releases/content/net/ihe/gazelle/gwt-message-recording-for-mock/1.7.3/gwt-message-recording-for-mock-1.7.3-jar-with-dependencies.jar
```

### Install libraries required by SoapUI

Get the external jars (esapi, velocity and postgresql)

```shell
cd $SOAPUI_INSTALL_DIR/lib/
wget https://repo1.maven.org/maven2/org/owasp/esapi/esapi/2.1.0.1/esapi-2.1.0.1.jar
wget https://repo1.maven.org/maven2/org/apache/velocity/velocity/1.7/velocity-1.7.jar
wget https://repo1.maven.org/maven2/org/postgresql/postgresql/42.2.22.jre7/postgresql-42.2.22.jre7.jar
```

### GWT Database

Assertion-Provider may use Gazelle-Webservice-Tester database to record exchanged messages. The 
database must be accessible at `jdbc:postgresql://localhost:5432/gazelle-webservice-tester` with
user `gazelle` and password `gazelle` by default.

If not configured, it will raise log errors for each recieving request.

### Keystore to sign generated SAML assertions

To sign generated assertion, the mock will need a certificate with its private key in a JKS keystore
at `"/opt/gazelle/cert/jboss.jks` with alias `jboss` and password `password` by default.

### Run the mock

Run with default parameters:

```shell
$SOAPUI_INSTALL_DIR/bin/mockservicerunner.sh $ASSERTION_PROVIDER_PROJECT_DIR/assertion-provider/soapui/epr-assertion-provider-soapui-project.xml >> /var/log/soapui/epr-assertion-provider.log 2>&1
```

Parameters can be changed using the following options:
* `-m` WSDL Mock name (default `STSBindingMockService`)
* `-p` Mock port (default 8090)
* `-a` Mock resource path (default `/STS`)
* `-PhomeCommunityID=` Assertion-Provider's Home Community ID (default is `urn:oid:1.1.4567334.1.6`)
* `-PjdbcUrl=` GWT database URL (default is `jdbc:postgresql://localhost:5432/gazelle-webservice-tester`)
* `-PdbUser=` User to connect to the database (default is `gazelle`)
* `-PdbUserPassword=` User's password to connect to the database (default is `gazelle`)

## Mock as a service

Perform all SoapUI, librairies, database and keystore configuration described above, but instead 
of directly calling the `mockRunner.sh` script, do the following steps:

### Prepare the init.d script

Edit the init.d script `$ASSERTION_PROVIDER_PROJECT_DIR/assertion-provider/init.d/assertionProviderMock` and set the following environment variables

* SOAPUI_PATH => Path of SoapUI folder
* SOAPUI_PROJECT_PATH => Path of SoapUI project script
* SOAPUI_MOCK_NAME => Name of the SoapUI mock
* SOAPUI_MOCK_PORT => Port of the SoapUI mock
* SOAPUI_MOCK_ENDPOINT => Path of the SoapUI mock
* SOAPUI_MOCK_LOG => Path where to publish log file
* HOME_COMMUNITY_ID => Home Community ID of the Assertion Provider

### Declare the service

Type the following commands register the init.d script as service

```bash
sudo cp $EPR_AP_MOCK_DIR/init.d/assertionProviderMock /etc/init.d/assertionProviderMock
sudo chmod u+x /etc/init.d/assertionProviderMock
sudo chmod 775 /etc/init.d/assertionProviderMock
```

If you want the service to start at each machine start up

```bash
sudo update-rc.d assertionProviderMock defaults
```

Be careful to allow the service to write logs into your target directory. As example

```bash
sudo mkdir /var/log/soapui
sudo chmod 775 /var/log/soapui
```

### Start the mock

To run the mock

```bash
sudo /etc/init.d/assertionProviderMock start
```

To stop the mock

```bash
sudo /etc/init.d/assertionProviderMock stop
```

To get status of the mock

```bash
sudo /etc/init.d/assertionProviderMock status
```


## Troubleshouting

### Missing packets

You might need to install those following packets

```bash
sudo apt-get install -y libxrender1 libxtst6 libxi6
```

### Plugins error at startup

You might need to resolve plugins errors when starting the mock

```bash
sudo mv $USER_DIR/.soapuios/ $USER_DIR/.soapuios_old
```

If lunch as a service or as root
```bash
sudo mv /root/.soapuios/ /root/.soapuios_old
```
