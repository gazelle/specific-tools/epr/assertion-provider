package net.ihe.gazelle.xua.generator.utils;

import net.ihe.gazelle.xua.generator.keystoreutils.DefaultKeyStoreManager;
import net.ihe.gazelle.xua.generator.keystoreutils.KeyStoreManager;
import net.ihe.gazelle.xua.generator.model.AssertionAttributes;
import net.ihe.gazelle.xua.generator.model.KeystoreParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class AssertionUtils {

    private static final Logger LOG = LoggerFactory.getLogger(AssertionUtils.class);

    public static String getStringAssertionWithPath(AssertionAttributes assertionAttributes, KeystoreParams keystoreParams) throws Exception {
        KeyStoreManager keystoreObject = new DefaultKeyStoreManager(keystoreParams);
        try {
            keystoreObject.generateStores();
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            throw e;
        }
        return EhealthsuisseHelperService.getStringAssertion(keystoreObject, keystoreParams, assertionAttributes);
    }
}
