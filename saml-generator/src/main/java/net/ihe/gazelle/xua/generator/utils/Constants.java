package net.ihe.gazelle.xua.generator.utils;

/**
 * <p>AttributeNames class.</p>
 *
 * @author aberge
 * @version 1.0: 26/09/17
 */

final class Constants {

    static final String SUBJECT_ID = "urn:oasis:names:tc:xspa:1.0:subject:subject-id";
    static final String SUBJECT_ROLE = "urn:oasis:names:tc:xacml:2.0:subject:role";
    static final String SUBJECT_ORGANIZATION = "urn:oasis:names:tc:xspa:1.0:subject:organization";
    static final String SUBJECT_ORGANIZATION_ID = "urn:oasis:names:tc:xspa:1.0:subject:organization-id";
    static final String RESOURCE_RESOURCE_ID = "urn:oasis:names:tc:xacml:2.0:resource:resource-id";
    static final String SUBJECT_PURPOSEOFUSE = "urn:oasis:names:tc:xspa:1.0:subject:purposeofuse";
    static final String AUDIENCE_ALL_COMMUNITIES = "urn:e-health-suisse:token-audience:all-communities";
    static final String HOME_COMMUNITY_ID = "urn:ihe:iti:xca:2010:homeCommunityId";
    static final String CODE_SYSTEM_NAME = "codeSystemName";
    static final String DISPLAY_NAME = "displayName";
    static final String CODE_SYSTEM = "codeSystem";
    static final String ISSUER = "https://ehealthsuisse.ihe-europe.net/STS";
    static final String V3_NAMESPACE = "urn:hl7-org:v3";
    static final String CODE = "code";
    static final String SIGNATURE_ALGORITHM = "http://www.w3.org/2000/09/xmldsig#rsa-sha1";
    static final String CANONICALIZATION_ALGORITHM = "http://www.w3.org/2001/10/xml-exc-c14n#";
    static final String AUTHN_CONTEXT_CLASS_REF = "http://bag.admin.ch/LoA/3";

    private Constants() {

    }


}
