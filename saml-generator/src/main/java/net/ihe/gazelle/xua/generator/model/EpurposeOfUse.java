package net.ihe.gazelle.xua.generator.model;

/**
 * <p>EpurposeOfUse enum.</p>
 *
 * @author aberge
 * @version 1.0: 26/09/17
 */
public enum EpurposeOfUse implements CEDatatype {

    NORM("NORM", "Normalzugriff"),
    AUTO("AUTO", "Automatic Upload of non DICOM Documents"),
    DICOM_AUTO("DICOM_AUTO", "Automatic Upload of DICOM Contents"),
    EMER("EMER", "Emergency");

    String code;
    String displayName;

    EpurposeOfUse(String code, String displayName) {
        this.code = code;
        this.displayName = displayName;
    }

    public static EpurposeOfUse getEpurposeOfUseByCode(String inCode) {
        for (EpurposeOfUse value : values()) {
            if (value.getCode().equals(inCode)) {
                return value;
            }
        }
        return null;
    }

    public String getCode() {
        return code;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getHL7v3Name() {
        return "PurposeOfUse";
    }

    public String getCodeSystem() {
        return "eHealth Suisse Verwendungszweck";
    }

    public String getCodeSystemName() {
        return "2.16.756.5.30.1.127.3.10.5";
    }
}
