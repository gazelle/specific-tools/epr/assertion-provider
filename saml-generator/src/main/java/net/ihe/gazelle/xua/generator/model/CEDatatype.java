package net.ihe.gazelle.xua.generator.model;

/**
 * <p>CEDatatype interface.</p>
 *
 * @author aberge
 * @version 1.0: 26/09/17
 */
public interface CEDatatype {

    String getCode();

    String getDisplayName();

    String getHL7v3Name();

    String getCodeSystem();

    String getCodeSystemName();
}
