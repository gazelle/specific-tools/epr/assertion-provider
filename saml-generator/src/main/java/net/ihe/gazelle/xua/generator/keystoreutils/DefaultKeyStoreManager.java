package net.ihe.gazelle.xua.generator.keystoreutils;
/*
 *  Copyright 2010 jerouris.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */


import net.ihe.gazelle.xua.generator.model.KeystoreParams;
import net.ihe.gazelle.xua.generator.utils.SMgrException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;

/**
 * @author jerouris
 */
public final class DefaultKeyStoreManager implements KeyStoreManager {

    private static final Logger logger = LoggerFactory.getLogger(DefaultKeyStoreManager.class);

    private String keystoreLocation;
    private String truststoreLocation;
    private String keystorePassword;
    private String truststorePassword;
    private String privatekeyAlias;
    private String privatekeyPassword;
    private KeyStore keyStore;
    private KeyStore trustStore;


    public DefaultKeyStoreManager(String keystorePath, String truststorePath, String privateKeyPassword, String privateKeyAlias) throws Exception {

        // Constants Initialization
        keystoreLocation = keystorePath;
        truststoreLocation = truststorePath;
        keystorePassword = privateKeyPassword;
        truststorePassword = privateKeyPassword;
        privatekeyAlias = privateKeyAlias;
        privatekeyPassword = privateKeyAlias;
    }

    public DefaultKeyStoreManager(KeystoreParams params) throws Exception {
        this.keystoreLocation = params.getKeystorePath();
        this.truststoreLocation = params.getTruststorePath();
        this.keystorePassword = params.getKeystorePassword();
        this.truststorePassword = params.getPrivateKeyPassword();
        this.privatekeyAlias = params.getKeyAlias();
        this.privatekeyPassword = params.getPrivateKeyPassword();
    }

    public void generateStores() throws Exception {
        this.keyStore = getKeyStore();
        this.trustStore = getTrustStore();
    }


    public KeyPair getPrivateKey(String alias, char[] password) throws SMgrException {

        try {

            // Get private key
            Key key = keyStore.getKey(alias, password);
            if (key instanceof PrivateKey) {
                // Get certificate of public key
                java.security.cert.Certificate cert = keyStore.getCertificate(alias);

                // Get public key
                PublicKey publicKey = cert.getPublicKey();

                // Return a key pair
                return new KeyPair(publicKey, (PrivateKey) key);
            }
        } catch (UnrecoverableKeyException e) {
            logger.error(null, e);
            throw new SMgrException("Key with alias:" + alias + " is unrecoverable", e);
        } catch (NoSuchAlgorithmException e) {
            logger.error(null, e);
            throw new SMgrException("Key with alias:" + alias + " uses an incompatible algorithm", e);
        } catch (KeyStoreException e) {
            logger.error(null, e);
            throw new SMgrException("Key with alias:" + alias + " not found", e);
        }
        return null;
    }

    public KeyStore getKeyStore() throws Exception {
        keyStore = buildKeyStore(keystoreLocation, keystorePassword);
        return keyStore;
    }

    public Certificate getCertificate(String alias) throws SMgrException {
        try {
            java.security.cert.Certificate cert = keyStore.getCertificate(alias);
            return cert;
        } catch (KeyStoreException ex) {
            logger.error(null, ex);
            throw new SMgrException("Certificate with alias: " + alias + " not found in keystore", ex);
        }
    }

    public KeyStore getTrustStore() throws Exception {
        trustStore = buildKeyStore(truststoreLocation, truststorePassword);
        return trustStore;
    }

    public KeyPair getDefaultPrivateKey() throws SMgrException {
        return getPrivateKey(privatekeyAlias, privatekeyPassword.toCharArray());
    }

    public Certificate getDefaultCertificate() throws SMgrException {
        return getCertificate(privatekeyAlias);
    }

    private KeyStore buildKeyStore(String location, String password) throws Exception {
        KeyStore store = null;
        InputStream keystoreStream = null;
        try {
            store = KeyStore.getInstance(KeyStore.getDefaultType());
            keystoreStream = new FileInputStream(location);
            store.load(keystoreStream, password.toCharArray());
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            throw ex;
        } finally {
            if (keystoreStream != null) {
                try {
                    keystoreStream.close();
                } catch (IOException e) {
                    logger.error("Unable to close stream", e);
                }
            }
        }
        return store;
    }
}