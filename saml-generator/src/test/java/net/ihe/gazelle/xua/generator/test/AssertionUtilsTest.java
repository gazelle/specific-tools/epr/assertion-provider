package net.ihe.gazelle.xua.generator.test;

import net.ihe.gazelle.xua.generator.model.AssertionAttributes;
import net.ihe.gazelle.xua.generator.model.EpurposeOfUse;
import net.ihe.gazelle.xua.generator.model.KeystoreParams;
import net.ihe.gazelle.xua.generator.utils.AssertionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AssertionUtilsTest {

    private static final Logger LOG = LoggerFactory.getLogger(AssertionUtilsTest.class);

    private static final String EPR_S_PID = "761337610435209810^^^SPID&2.16.756.5.30.1.127.3.10.3&ISO";
    private static final String GLN = "7601000080776";
    private static final String PURPOSE_OF_USE = EpurposeOfUse.EMER.getCode();
    private static final String KEYSTORE_PATH = "/opt/gazelle/cert/jboss.jks";
    private static final String TRUSTSTORE_PATH = "/opt/gazelle/cert/jboss.jks";
    private static final String PRIVATE_KEY_PASSWORD = "password";
    private static final String KEY_ALIAS = "jboss";

    public static void main(String[] args) {
        AssertionAttributes attributes = getAssertionAttributes();
        KeystoreParams keystoreParams = getKeystoreParameters();
        try {
            String assertion = AssertionUtils.getStringAssertionWithPath(attributes, keystoreParams);
            System.out.println(assertion);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
    }

    private static AssertionAttributes getAssertionAttributes() {
        return new AssertionAttributes(GLN, EPR_S_PID, PURPOSE_OF_USE, "test", "11", "name", "HCP",
                "1", "ASS", "aaa", "urn:oid:1.2.3.4");
    }

    private static KeystoreParams getKeystoreParameters() {
        return new KeystoreParams(KEYSTORE_PATH, PRIVATE_KEY_PASSWORD, TRUSTSTORE_PATH, PRIVATE_KEY_PASSWORD, KEY_ALIAS, PRIVATE_KEY_PASSWORD);
    }
}
