import net.ihe.gazelle.xua.generator.utils.AssertionUtils
import net.ihe.gazelle.xua.generator.model.AssertionAttributes;
import net.ihe.gazelle.xua.generator.model.KeystoreParams;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.eviware.soapui.support.XmlHolder

/////////////////////////////////////////
// CONSTANTS ////////////////////////////
/////////////////////////////////////////

def final DEFAULT_HOME_COMMUNITY_ID = "urn:oid:1.1.4567334.1.6"
def final PROPERTY_HOME_COMMUNITY_ID = "homeCommunityID"

def final DEFAULT_KEYSTORE_PATH = "/opt/gazelle/cert/jboss.jks"
def final DEFAULT_KEYSTORE_PASSWORD = "password"
def final DEFAULT_KEY_ALIAS = "jboss"
def final DEFAULT_KEY_PASSWORD = "password"
def final DEFAULT_TRUSTSTORE_PATH = DEFAULT_KEYSTORE_PATH
def final DEFAULT_TRUSTSTORE_PASSWORD = DEFAULT_KEYSTORE_PASSWORD

/////////////////////////////////////////
// MOCK PARAMETERS //////////////////////
/////////////////////////////////////////

// Home Community ID
def homeCommunityID = getParameter(PROPERTY_HOME_COMMUNITY_ID, DEFAULT_HOME_COMMUNITY_ID);

// Keystore
def keystoreParams = new KeystoreParams(DEFAULT_KEYSTORE_PATH, DEFAULT_KEYSTORE_PASSWORD, DEFAULT_TRUSTSTORE_PATH, DEFAULT_TRUSTSTORE_PASSWORD, DEFAULT_KEY_ALIAS, DEFAULT_KEY_PASSWORD)

//////////////////////////////////////////
// create XmlHolder for request content //
//////////////////////////////////////////
def holder = new XmlHolder(mockRequest.requestContent)
holder.declareNamespace("wst", "http://docs.oasis-open.org/ws-sx/ws-trust/200512")
holder.declareNamespace("wsp", "http://schemas.xmlsoap.org/ws/2004/09/policy")
holder.declareNamespace("wsa", "http://www.w3.org/2005/08/addressing")
holder.declareNamespace("saml2", "urn:oasis:names:tc:SAML:2.0:assertion")
holder.declareNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance")

/////////////////////////////////////////
//CHECK PROPERTIERS FOR GENERAL REQUEST//
/////////////////////////////////////////
def g_purposeOfUse = holder["//wst:Claims/saml2:Attribute[@Name='urn:oasis:names:tc:xspa:1.0:subject:purposeofuse']/saml2:AttributeValue/*:PurposeOfUse/@code"]
def g_role = holder["//wst:Claims/saml2:Attribute[@Name='urn:oasis:names:tc:xacml:2.0:subject:role']/saml2:AttributeValue/*:Role/@code"]
def g_EPR_SPIP = holder["//wst:Claims/saml2:Attribute[@Name='urn:oasis:names:tc:xacml:2.0:resource:resource-id']/saml2:AttributeValue"]
def g_assertion = holder["exists(//saml2:Assertion)"]
def applies_to_exist = holder["exists(//wst:RequestSecurityToken/wsp:AppliesTo)"]

if (g_purposeOfUse != null && isDefined(g_purposeOfUse)) {
    requestContext.g_purposeOfUse = g_purposeOfUse
} else {
    return soapFaultMissing("Purpose of use")
}
if (g_role != null && isDefined(g_role)) {
    requestContext.g_role = g_role
} else {
    return soapFaultMissing("Role")
}
if (g_assertion != null && isDefined(g_assertion)) {
    requestContext.g_assertion = g_assertion
} else {
    return soapFaultMissing("Assertion")
}

if (g_EPR_SPIP != null && isDefined(g_EPR_SPIP)) {
    requestContext.g_EPR_SPIP = g_EPR_SPIP
    if (!isValidPatientId(g_EPR_SPIP)) {
        return soapFaultWrongValue("Resource ID", g_EPR_SPIP)
    }
} else {
    return soapFaultMissing("Resource ID")
}


log.info "All checks for common requirements passed"

/////////////////////////////////////////
/////////////CHECK ROLE CODE/////////////
/////////////////////////////////////////

log.info "Check role code with value : " + g_role
g_role = checkRoleCode(g_role)

if (g_role == "SoapFault Response") {
    return g_role
}


/////////////////////////////////////////
///SPECIFIC CHECK BASED ON ROLE CODE/////
/////////////////////////////////////////


if (g_role == "HCP") {
    log.info "Role detected : Healthcare Professional"
    def hcp_assertion_gln = holder["//saml2:Assertion//saml2:AttributeStatement//saml2:Attribute[@NameFormat='urn:oasis:names:tc:ebcore:partyid-type:DataUniversalNumberingSystem:0060' or @Name='GLN']/saml2:AttributeValue"]
    def hcp_id_subject = holder["//saml2:Subject/saml2:NameID"]

    if (isValidPurposeOfUse(g_purposeOfUse)) {
        requestContext.g_purposeOfUse = g_purposeOfUse
    } else {
        return soapFaultWrongValue("Purpose of use", g_purposeOfUse)
    }

    if (isDefined(hcp_assertion_gln)) {
        requestContext.final_gln = hcp_assertion_gln
        if (!isGlnInDataStore(hcp_assertion_gln)) {
            return soapFaultWrongValue("GLN from assertion", hcp_assertion_gln)
        }
        log.info "Found GLN in assertion : " + requestContext.final_gln
    } else {
        //TODO query the community data stores to resolve the Name ID of the <Subject> element to the GLN of the healthcare professional to be returned in the <Assertion>
        def gnl_from_nameid = getGlnFromNameId(hcp_id_subject)
        if (gnl_from_nameid == null) {
            return soapFaultWrongValue("Subject ID", hcp_id_subject)
        }
        requestContext.final_gln = gnl_from_nameid
        log.info "Found GLN from subject ID : " + requestContext.final_gln
    }

    //TODO proof that the GLN of the healthcare professional is registered in the Provider Information Directory ???

    //TODO query the Healthcare Provider Directory and resolve the GLN of the healthcare professional to all groups including all
    //superior group up to the root level. The X-Asser-tion Provider actor must add the group IDs and the group names in an ordered sequence
    def groupId = getGroupIdFromHcpGln(requestContext.final_gln)
    if (groupId == null) {
        log.info "No Group id for GLN with value : " + requestContext.final_gln
        requestContext.soapFaultCodeValue = "wst:InvalidRequest"
        requestContext.soapFaultReason = "No Group id for GLN with value : " + requestContext.final_gln
        return "SoapFault Response";
    }
    def organizationName = getOrganizationNameFromId(groupId);
    if (organizationName == null) {
        log.info "No organization name for id : " + groupId
        requestContext.soapFaultCodeValue = "wst:InvalidRequest"
        requestContext.soapFaultReason = "No organization name for id : " + groupId
        return "SoapFault Response";
    }

    def doctorName = getNameFromGln(requestContext.final_gln)
    if (doctorName == null) {
        log.info "No name for GLN : " + requestContext.final_gln
        requestContext.soapFaultCodeValue = "wst:InvalidRequest"
        requestContext.soapFaultReason = "No name for GLN : " + requestContext.final_gln
        return "SoapFault Response";
    }

    // Generate assertion
    log.info "Generating assertion"
    def assertionAttributes = new AssertionAttributes(requestContext.final_gln, requestContext.g_EPR_SPIP, requestContext.g_purposeOfUse, doctorName, groupId, organizationName, g_role, null, null, null, homeCommunityID)
    def assertion = AssertionUtils.getStringAssertionWithPath(assertionAttributes, keystoreParams)
    requestContext.appliesTo = getAppliesTo(applies_to_exist, holder)
    requestContext.assertion = assertion
    return "Default RequestSecurityTokenResponse"


} else if (g_role == "ASS") {
    log.info "Role detected : Assistant"
    //MUST FOR ASS
    // FROM CLAIMS
    def ass_principalID = holder["//wst:Claims/saml2:Attribute[@Name='urn:e-health-suisse:principal-id']/saml2:AttributeValue"]
    def ass_principalName = holder["//wst:Claims/saml2:Attribute[@Name='urn:e-health-suisse:principal-name']/saml2:AttributeValue"]

    def ass_organizationName = holder["//wst:Claims/saml2:Attribute[@Name='urn:oasis:names:tc:xspa:1.0:sub-ject:organization']/saml2:AttributeValue"]
    def ass_organizationId = holder["//wst:Claims/saml2:Attribute[@Name='urn:oasis:names:tc:xspa:1.0:sub-ject:organization-id']/saml2:AttributeValue"]

    // FROM ASSERTION
    def ass_assertion_gln = holder["//saml2:Assertion//saml2:AttributeStatement//saml2:Attribute[@NameFormat='urn:oasis:names:tc:ebcore:partyid-type:DataUniversalNumberingSystem:0060' or @Name='GLN']/saml2:AttributeValue"]
    def ass_id_subject = holder["//saml2:Subject/saml2:NameID"]
    def ass_assertion_idOrganization = holder["//saml2:AttributeStatement//saml2:Attribute[@Name='urn:oasis:names:tc:xspa:1.0:subject:organization-id']/saml2:AttributeValue"]
    def ass_assertion_nameOrganization = holder["//saml2:AttributeStatement//saml2:Attribute[@Name='urn:oasis:names:tc:xspa:1.0:subject:organization']/saml2:AttributeValue"]
    //def ass_claims_idOrganization = holder["//wst:Claims//saml2:Attribute[@Name='urn:oasis:names:tc:xspa:1.0:subject:organization-id']/saml2:AttributeValue"]
    def ass_name_access = holder["//saml2:AttributeStatement//saml2:Attribute[@Name='urn:oasis:names:tc:xspa:1.0:subject:subject-id']/saml2:AttributeValue"]

    /////////////////////////////////////////////////////////
    log.info "Check MUST fields :"

    if (isDefined(ass_principalID)) {
        requestContext.ass_principalID = ass_principalID
        if (!isGlnInDataStore(requestContext.ass_principalID)) {
            log.info "No entry for GLN in data stores : " + requestContext.ass_principalID
            requestContext.soapFaultCodeValue = "wst:InvalidRequest"
            requestContext.soapFaultReason = "No entry for GLN : " + requestContext.ass_principalID
            return "SoapFault Response";
        }
    } else {
        return soapFaultMissing("Principal ID")
    }

    if (!isDefined(ass_principalName)) {
        return soapFaultMissing("Principal Name")
    }

    if (isValidPurposeOfUse(g_purposeOfUse)) {
        log.info "Valid purpose of use with value : " + g_purposeOfUse
        requestContext.g_purposeOfUse = g_purposeOfUse
    } else {
        return soapFaultWrongValue("Purpose of use", g_purposeOfUse)
    }

    log.info "All MUST checked for assistant request"

    ///////////////////////////////////////////////////////////////
    log.info "Check optional fields :"

    if (isDefined(ass_organizationName)) {
        if (!ass_organizationName.equals(getOrganizationNameFromId(getGroupIdFromHcpGln(requestContext.ass_principalID)))) {
            log.info "Organization name does not correspond to GLN : " + ass_organizationName
            requestContext.soapFaultCodeValue = "wst:InvalidRequest"
            requestContext.soapFaultReason = "Organization name does not correspond to GLN : " + ass_organizationName
            return "SoapFault Response";
        }
        requestContext.ass_organizationName = ass_organizationName
    }

    if (isDefined(ass_organizationId)) {
        if (!ass_organizationId.equals(getGroupIdFromHcpGln(requestContext.ass_principalID))) {
            log.info "Organization ID does not correspond to GLN : " + ass_organizationId
            requestContext.soapFaultCodeValue = "wst:InvalidRequest"
            requestContext.soapFaultReason = "Organization ID does not correspond to GLN : " + ass_organizationId
            return "SoapFault Response";
        }
    }

    log.info "All optional fields have been checked for assistant request"
    ///////////////////////////////////////////////////////////////
    log.info "Check values to put in the assertion : "

    // verify Id of the accessing person
    if (ass_id_subject.size() > 0) {
        log.info "Id of the accessing person: " + ass_id_subject
        requestContext.id_subject = ass_id_subject
    } else {
        return soapFaultMissing("Id of the accessing person")
    }


    if (isDefined(ass_assertion_gln)) {
        requestContext.ass_gln = ass_assertion_gln
    } else {
        requestContext.ass_gln = getGlnFromAssistantId(requestContext.id_subject)
    }

    if (requestContext.ass_gln == null) {
        return soapFaultWrongValue("Assistant GLN", requestContext.ass_gln)
    }

    if (!isAssistantAuthorized(requestContext.ass_gln, requestContext.ass_principalID)) {
        log.info "Assistant with gln " + requestContext.ass_gln + " not authorized to act for HCP with ID " + requestContext.ass_principalID
        requestContext.soapFaultCodeValue = "wst:InvalidRequest"
        requestContext.soapFaultReason = "Assistant with gln " + requestContext.ass_gln + " not authorized to act for HCP with ID " + requestContext.ass_principalID
        return "SoapFault Response"
    }

    if (getNameFromGln(requestContext.ass_principalID) == null) {
        log.info "No name for gln " + requestContext.ass_principalID
        requestContext.soapFaultCodeValue = "wst:InvalidRequest"
        requestContext.soapFaultReason = "No name for gln " + requestContext.ass_principalID
        return "SoapFault Response"
    }
    if (getGroupIdFromHcpGln(requestContext.ass_principalID) == null) {
        log.info "No organization for gln " + requestContext.ass_principalID
        requestContext.soapFaultCodeValue = "wst:InvalidRequest"
        requestContext.soapFaultReason = "No organization for gln " + requestContext.ass_principalID
        return "SoapFault Response"
    }
    if (getOrganizationNameFromId(getGroupIdFromHcpGln(requestContext.ass_principalID)) == null) {
        log.info "No organization name for gln " + requestContext.ass_principalID
        requestContext.soapFaultCodeValue = "wst:InvalidRequest"
        requestContext.soapFaultReason = "No organization name for gln " + requestContext.ass_principalID
        return "SoapFault Response"
    }


    // Generate assertion
    log.info "Generating assertion"

    def assertionAttributes = new AssertionAttributes(requestContext.ass_principalID, requestContext.g_EPR_SPIP, requestContext.g_purposeOfUse,
            getNameFromGln(requestContext.ass_principalID), getGroupIdFromHcpGln(requestContext.ass_principalID),
            getOrganizationNameFromId(getGroupIdFromHcpGln(requestContext.ass_principalID)), "HCP", requestContext.ass_gln, "ASS", getAssNameFromGLN(requestContext.ass_gln), homeCommunityID)
    def assertion = AssertionUtils.getStringAssertionWithPath(assertionAttributes, keystoreParams)
    requestContext.appliesTo = getAppliesTo(applies_to_exist, holder)
    requestContext.assertion = assertion
    return "Default RequestSecurityTokenResponse"

} else if (g_role == "TCU") {

    log.info "Role detected : Technical User"

    //MUST FOR TCU
    def tcu_principalID = holder["//wst:Claims/saml2:Attribute[@Name='urn:e-health-suisse:principal-id']/saml2:AttributeValue"]
    def tcu_principalName = holder["//wst:Claims/saml2:Attribute[@Name='urn:e-health-suisse:principal-name']/saml2:AttributeValue"]
    //Infos from assertion
    def tcu_assertion_idOrganization = holder["//saml2:AttributeStatement//saml2:Attribute[@Name='urn:oasis:names:tc:xspa:1.0:subject:organization-id']/saml2:AttributeValue"]
    def tcu_assertion_nameOrganization = holder["//saml2:AttributeStatement//saml2:Attribute[@Name='urn:oasis:names:tc:xspa:1.0:subject:organization']/saml2:AttributeValue"]
    //def ass_claims_idOrganization = holder["//wst:Claims//saml2:Attribute[@Name='urn:oasis:names:tc:xspa:1.0:subject:organization-id']/saml2:AttributeValue"]
    def tcu_name_access = holder["//saml2:AttributeStatement//saml2:Attribute[@Name='urn:oasis:names:tc:xspa:1.0:subject:subject-id']/saml2:AttributeValue"]


    def tcu_id_subject = holder["//saml2:Subject/saml2:NameID"]

    if (isDefined(tcu_principalID)) {
        requestContext.tcu_principalID = tcu_principalID
        if (!isGlnInDataStore(requestContext.tcu_principalID)) {
            log.info "No entry for GLN in data stores : " + requestContext.tcu_principalID
            requestContext.soapFaultCodeValue = "wst:InvalidRequest"
            requestContext.soapFaultReason = "No entry for GLN : " + requestContext.tcu_principalID
            return "SoapFault Response";
        }
    } else {
        return soapFaultMissing("Principal ID")
    }

    if (!isDefined(tcu_principalName)) {
        return soapFaultMissing("Principal Name")
    }


    if (g_purposeOfUse == "AUTO") {
        requestContext.g_purposeOfUse = g_purposeOfUse
    } else {
        return soapFaultWrongValue("Purpose of use", g_purposeOfUse)
    }


    //TODO  use the Name ID of the <Subject> element and query the community data stores for
    //the X.509 certificate registered with the technical user.
    //TODO authenticate the technical user by validating the signature of the Assertion with
    //the certificate registered with the technical user.


    if (isDefined(tcu_id_subject)) {
        def tcu_gln = getTcuGlnFromNameId(tcu_id_subject)
        if (!isTechnicalUserAuthorized(tcu_gln, requestContext.tcu_principalID)) {
            log.info "Technical user with id " + tcu_gln + " not authorized to act for HCP with ID " + requestContext.tcu_principalID
            requestContext.soapFaultCodeValue = "wst:InvalidRequest"
            requestContext.soapFaultReason = "Technical user with id " + tcu_gln + " not authorized to act for HCP with ID " + requestContext.tcu_principalID
            return "SoapFault Response"
        }
        requestContext.tcu_gln = tcu_gln
    } else {
        return soapFaultMissing("Subject ID")
    }

    if (getTcuGlnFromNameId(tcu_id_subject) == null) {
        return soapFaultWrongValue("Name ID", tcu_id_subject)
    }

    //HCP NAME
    if (getNameFromGln(requestContext.tcu_principalID) == null) {
        log.info "No name for gln" + requestContext.tcu_principalID
        requestContext.soapFaultCodeValue = "wst:InvalidRequest"
        requestContext.soapFaultReason = "No name for gln " + requestContext.tcu_principalID
        return "SoapFault Response"
    }
    //ORGA ID
    if (getGroupIdFromHcpGln(requestContext.tcu_principalID) == null) {
        log.info "No organization for gln" + requestContext.tcu_principalID
        requestContext.soapFaultCodeValue = "wst:InvalidRequest"
        requestContext.soapFaultReason = "No organization for gln " + requestContext.tcu_principalID
        return "SoapFault Response"
    }
    //ORGA NAME
    if (getOrganizationNameFromId(getGroupIdFromHcpGln(requestContext.tcu_principalID)) == null) {
        log.info "No organization name for gln" + requestContext.tcu_principalID
        requestContext.soapFaultCodeValue = "wst:InvalidRequest"
        requestContext.soapFaultReason = "No organization name for gln " + requestContext.tcu_principalID
        return "SoapFault Response"
    }


    //////////////////////////////////////////////////////
    //Generate Assertion

    log.info "Generating assertion"
    def assertionAttributes = new AssertionAttributes(requestContext.tcu_principalID, requestContext.g_EPR_SPIP, "AUTO",
            getNameFromGln(requestContext.tcu_principalID), getGroupIdFromHcpGln(requestContext.tcu_principalID),
            getOrganizationNameFromId(getGroupIdFromHcpGln(requestContext.tcu_principalID)), "HCP", requestContext.tcu_gln, "TCU", null, homeCommunityID)

    def assertion = AssertionUtils.getStringAssertionWithPath(assertionAttributes, keystoreParams)
    requestContext.appliesTo = getAppliesTo(applies_to_exist, holder)
    requestContext.assertion = assertion
    return "Default RequestSecurityTokenResponse"

} else if (g_role == "PADM") {

    log.info "Role detected : Policy Administrator"
    def padm_id_subject = holder["//saml2:Subject/saml2:NameID"]

    if (g_purposeOfUse == "NORM") {
        requestContext.g_purposeOfUse = g_purposeOfUse
    } else {
        return soapFaultWrongValue("Purpose of use", g_purposeOfUse)
    }

    if (isDefined(padm_id_subject)) {
        def padm_gln = getPadmGlnFromNameId(padm_id_subject)
        if (padm_gln == null) {
            return soapFaultWrongValue("Name id in community store", padm_id_subject)
        }
        requestContext.padm_gln = padm_gln
    } else {
        return soapFaultMissing("Subject ID")
    }

    log.info "Generating assertion"
    def assertionAttributes = new AssertionAttributes(requestContext.padm_gln, requestContext.g_EPR_SPIP, "NORM", getPADMNameFromId(padm_id_subject), null, null, "PADM", null, null, null, homeCommunityID)
    def assertion = AssertionUtils.getStringAssertionWithPath(assertionAttributes, keystoreParams);
    requestContext.appliesTo = getAppliesTo(applies_to_exist, holder)
    requestContext.assertion = assertion
    return "Default RequestSecurityTokenResponse"


} else if (g_role == "DADM") {
    log.info "Role detected : Document Administrator"
    def dadm_id_subject = holder["//saml2:Subject/saml2:NameID"]

    if (g_purposeOfUse == "NORM") {
        requestContext.g_purposeOfUse = g_purposeOfUse
    } else {
        return soapFaultWrongValue("Purpose of use", g_purposeOfUse)
    }

    if (isDefined(dadm_id_subject)) {
        def dadm_gln = getDadmGlnFromNameId(dadm_id_subject)
        if (dadm_gln == null) {
            return soapFaultWrongValue("Name id in community store", dadm_id_subject)
        }
        requestContext.dadm_gln = dadm_gln
    } else {
        return soapFaultMissing("Subject ID")
    }

    log.info "Generating assertion"
    def assertionAttributes = new AssertionAttributes(requestContext.dadm_gln, requestContext.g_EPR_SPIP, "NORM", getDADMNameFromId(dadm_id_subject), null, null, g_role, null, null, null, homeCommunityID)
    def assertion = AssertionUtils.getStringAssertionWithPath(assertionAttributes, keystoreParams);
    requestContext.appliesTo = getAppliesTo(applies_to_exist, holder)
    requestContext.assertion = assertion
    return "Default RequestSecurityTokenResponse"

} else if (g_role == "PAT") {
    log.info "Role detected : Patient"
    def pat_principalID = holder["//wst:Claims/saml2:Attribute[@Name='urn:e-health-suisse:principal-id']/saml2:AttributeValue"]
    def pat_principalName = holder["//wst:Claims/saml2:Attribute[@Name='urn:e-health-suisse:principal-name']/saml2:AttributeValue"]

    def pat_id_subject = holder["//saml2:Subject/saml2:NameID"]

    if (g_purposeOfUse == "NORM") {
        requestContext.g_purposeOfUse = g_purposeOfUse
    } else {
        return soapFaultWrongValue("Purpose of use", g_purposeOfUse)
    }

    if (isDefined(pat_id_subject)) {
        log.info "Subject id : " + pat_id_subject
        requestContext.pat_id_subject = pat_id_subject
        def patientid_from_nameid = getPatientIdFromNameId(pat_id_subject)
        if (patientid_from_nameid == null) {
            return soapFaultWrongValue("Name id in community store", pat_id_subject)
        }
        requestContext.patientid_from_nameid = patientid_from_nameid
    } else {
        return soapFaultMissing("Subject ID")
    }


    log.info "Generating assertion"
    def assertionAttributes = new AssertionAttributes(requestContext.patientid_from_nameid, requestContext.g_EPR_SPIP, g_purposeOfUse, getPatientNameFromId(requestContext.pat_id_subject), null, null, g_role, null, null, null, homeCommunityID)
    def assertion = AssertionUtils.getStringAssertionWithPath(assertionAttributes, keystoreParams);
    requestContext.appliesTo = getAppliesTo(applies_to_exist, holder)
    requestContext.assertion = assertion
    return "Default RequestSecurityTokenResponse"


} else if (g_role == "REP") {
    log.info "Role detected : Representative"
    def rep_principalID = holder["//wst:Claims/saml2:Attribute[@Name='urn:e-health-suisse:principal-id']/saml2:AttributeValue"]
    def rep_principalName = holder["//wst:Claims/saml2:Attribute[@Name='urn:e-health-suisse:principal-name']/saml2:AttributeValue"]

    def rep_id_subject = holder["//saml2:Subject/saml2:NameID"]

    if (g_purposeOfUse == "NORM") {
        requestContext.g_purposeOfUse = g_purposeOfUse
    } else {
        return soapFaultWrongValue("Purpose of use", g_purposeOfUse)
    }

    if (isDefined(rep_id_subject)) {
        log.info "Subject id : " + rep_id_subject
        requestContext.rep_id_subject = rep_id_subject
        def patientid_from_nameid = getPatientIdFromNameId(rep_id_subject)
        if (patientid_from_nameid == null) {
            return soapFaultWrongValue("Name id in community store", rep_id_subject)
        }
        requestContext.patientid_from_nameid = patientid_from_nameid
    } else {
        return soapFaultMissing("Subject ID")
    }

    if (rep_id_subject == null) {
        return soapFaultMissing("Name ID")
    }

    log.info "Generating assertion"
    def assertionAttributes = new AssertionAttributes(requestContext.patientid_from_nameid, requestContext.g_EPR_SPIP, g_purposeOfUse, getRepNameFromId(rep_id_subject), null, null, g_role, null, null, null, homeCommunityID)
    def assertion = AssertionUtils.getStringAssertionWithPath(assertionAttributes, keystoreParams);
    requestContext.appliesTo = getAppliesTo(applies_to_exist, holder)
    requestContext.assertion = assertion
    return "Default RequestSecurityTokenResponse"


} else {
    log.info "No known role detected"
    return soapFaultWrongValue("Role", g_role)
}


/////////////////////////////////////////
/////////////////////////////////////////
///////////////METHODS///////////////////
/////////////////////////////////////////


boolean isDefined(String element) {
    return element.toString() != "[]" && element.size() > 0;
}

boolean isDefined(String[] element) {
    return isDefined(element.toString());
}


def getAppliesTo(def appliesToExist, def holder) {
    if (appliesToExist.contains("true")) {
        String appliesToName = holder["//wst:RequestSecurityToken/wsp:AppliesTo/wsa:EndpointReference/wsa:Address"]
        return "<wsp:AppliesTo>" +
                "<wsa:EndpointReference>" +
                "<wsa:Address>" + appliesToName + "</wsa:Address>" +
                "</wsa:EndpointReference>" +
                "</wsp:AppliesTo>"
    } else {
        return ""
    }
}

/////////////////////////////////////////
////////// PARAMETERS METHODS ///////////
/////////////////////////////////////////

def getParameter(def propertyName, def defaultValue) {
    def property = context.mockService.project.getPropertyValue(propertyName);
    return property != null && !property.isEmpty() ? property : defaultValue ;
}

/////////////////////////////////////////
/////////////////////////////////////////
/////////////IN DATA STORES//////////////
/////////////////////////////////////////

//Regex to check PatientId
boolean isValidPatientId(def value) {
    if (value == "") {
        return false;
    } else {
        log.info "Check SPID with value : " + value
        final String regex = "([a-zA-Z0-9]{1,})(\\^{3})([A-Z]{0,})(&|&amp;{1})([0-9\\.]{1,})(&|&amp;{1})(ISO{1})";
        final Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
        final Matcher matcher = pattern.matcher(value);
        if (matcher.find()) {
            log.info "Id has valid format"
            return isPatientIdInDataStore(value);
        } else {
            return false;
        }
    }
}

boolean isPatientIdInDataStore(def patientId) {
    return isInList(patientId, getPatientSpidList());
}

boolean isGlnInDataStore(String gln) {
    return isInList(gln, getGlnList());
}

boolean isInList(def element, def list) {
    return list.any { item -> item.equals(element) };
}

boolean isValidPurposeOfUse(String purposeOfUse) {
    return PurposeOfUse.getCodes().contains(purposeOfUse);
}

enum PurposeOfUse {
    NORMAL("NORM"),
    EMERGENCY("EMER"),
    AUTOMATIC_PROCESS("AUTO");

    private String code;

    private PurposeOfUse(String code) {
        this.code = code;
    }

    String getCode() {
        return code;
    }

    static String[] getCodes() {
        return PurposeOfUse.values().collect { it.getCode() };
    }
}

enum Role {
    PATIENT("PAT"),
    HEALTH_CARE_PRO("HCP"),
    ASSISTANT("ASS"),
    REPRESENTATIVE("REP"),
    POLICY_ADMIN("PADM"),
    TECHNICAL_USER("TCU"),
    DOCUMENT_ADMIN("DADM");

    private String code;

    private Role(String code) {
        this.code = code;
    }

    String getCode() {
        return code;
    }

    static String[] getCodes() {
        return Role.values().collect { it.getCode() }
    }

    static Role fromCode(String code) {
        Role.values().find { it.getCode() == code }
    }
}

String checkRoleCode(String g_role) {
    if (Role.getCodes().contains(g_role)) {
        return g_role;
    } else {
        return soapFaultWrongValue("Role", g_role)
    }
}


/////////////////////////////////////////
/////////////////////////////////////////
////////////////SOAP/////////////////////
/////////////////////////////////////////

String soapFaultMissing(String elementName) {
    log.info "Unable to get " + elementName + " from request";
    requestContext.soapFaultCodeValue = "wst:InvalidRequest";
    requestContext.soapFaultReason = "Unable to get " + elementName + " from request";
    return "SoapFault Response";
}

String soapFaultWrongValue(String elementName, String[] value) {
    log.info "Wrong value for " + elementName + " in request : " + value;
    requestContext.soapFaultCodeValue = "wst:InvalidRequest";
    requestContext.soapFaultReason = "Wrong value for " + elementName + " in request : " + value;
    return "SoapFault Response";
}

/////////////////////////////////////////
/////////////////////////////////////////
/////////////////////////////////////////
/////////////////////////////////////////

/////////////////////////////////////////
/////////////////////////////////////////
///////////MOCKED DATA GETTER////////////
/////////////////////////////////////////


String getPatientNameFromId(String pat_id_subject) {
    if (pat_id_subject.equals("wwalters")) {
        return "William Walters";
    }
    if (pat_id_subject.equals("bovie")) {
        return "Bergan Ovie";
    }
    if (pat_id_subject.equals("lavdic")) {
        return "Léo Gérard Avdic";
    }
    if (pat_id_subject.equals("maschwanden")) {
        return "Marie-Christelle Victoire Aschwanden-Stocker";
    }
    if (pat_id_subject.equals("gantonyova")) {
        return "Gebhard August Antonyova";
    }
    if (pat_id_subject.equals("aamrein")) {
        return "Alessandra Monica Amrein-Brunner";
    }
    if (pat_id_subject.equals("sebibi")) {
        return "Salome Anja Ebibi-Limani";
    }
    if (pat_id_subject.equals("negger")) {
        return "Nina Robine Egger-Staub";
    }
    if (pat_id_subject.equals("aerne")) {
        return "Andrea Juliana Erne Cehic";
    }
    if (pat_id_subject.equals("dozkanturk")) {
        return "Désirée Gabrielle Ôzkantürk";
    }
    if (pat_id_subject.equals("remery")) {
        return "Roman Remo Eméry";
    }
    if (pat_id_subject.equals("nwittwerchristen")) {
        return "Nilesh Wittwer-Christen";
    }
    return null;
}

String getPADMNameFromId(String padm_id_subject) {
    if (padm_id_subject.equals("icastineira")) {
        return "Ivo Castineira";
    }
    return null;
}

String getDADMNameFromId(String dadm_id_subject) {
    if (dadm_id_subject.equals("kweisskopf")) {
        return "Käthi Weisskopf";
    }
    return null;
}

String getRepNameFromId(String rep_id_subject) {
    return getPatientNameFromId(rep_id_subject);
}

String getAssNameFromGLN(String ass_gln) {
    return getNameFromGln(ass_gln);
}

String getGlnFromAssistantId(String assistantId) {
    if (assistantId.equals("ltieche")) {
        return "7601002467158";
    } else if (assistantId.equals("cberger")) {
        return "7601002466812";
    } else if (assistantId.equals("travasi")) {
        return "7601002462586";
    } else {
        return null;
    }
}

String getTcuGlnFromNameId(String tcu_id_subject) {
    if (tcu_id_subject.equals("sbaader")) {
        return "7601002461111";
    } else {
        return null;
    }
}

String getPadmGlnFromNameId(String padm_id_subject) {
    if (padm_id_subject.equals("icastineira")) {
        return "7601002468963";
    } else {
        return null;
    }
}

String getDadmGlnFromNameId(String dadm_id_subject) {
    if (dadm_id_subject.equals("kweisskopf")) {
        return "7601002467458";
    } else {
        return null;
    }
}

boolean isAssistantAuthorized(String ass_gln, String ass_principalID) {
    if (isInList(ass_gln, getGlnAssList())) {
        switch (ass_principalID) {
            case "7601002468282": return true;
            case "7601002467373": return true;
            default: return false;
        }
    }
    return false;
}

boolean isTechnicalUserAuthorized(String tcu_id, String ass_principalID) {
    if (isInList(tcu_id, getGlnTcuList())) {
        switch (ass_principalID) {
            case "7601000050717": return true;
            case "7601002466565": return true;
            default: return false;
        }
    }
    return false;
}


String[] getPatientSpidList() {

    return ["761337610435200998^^^SPID&2.16.756.5.30.1.127.3.10.3&ISO",
            "761337610435209810^^^SPID&2.16.756.5.30.1.127.3.10.3&ISO",
            "761337610436974489^^^SPID&2.16.756.5.30.1.127.3.10.3&ISO",
            "761337610430891416^^^SPID&2.16.756.5.30.1.127.3.10.3&ISO",
            "761337610423590456^^^SPID&2.16.756.5.30.1.127.3.10.3&ISO",
            "761337610455909127^^^SPID&2.16.756.5.30.1.127.3.10.3&ISO",
            "761337610445502987^^^SPID&2.16.756.5.30.1.127.3.10.3&ISO",
            "761337610448027647^^^SPID&2.16.756.5.30.1.127.3.10.3&ISO",
            "761337610469261945^^^SPID&2.16.756.5.30.1.127.3.10.3&ISO",
            "761337610510635763^^^SPID&2.16.756.5.30.1.127.3.10.3&ISO",
            "761337610433933946^^^SPID&2.16.756.5.30.1.127.3.10.3&ISO",
            "761337610411265456^^^SPID&2.16.756.5.30.1.127.3.10.3&ISO",
            "761337610411265789^^^SPID&2.16.756.5.30.1.127.3.10.3&ISO",
            "761337610411265777^^^SPID&2.16.756.5.30.1.127.3.10.3&ISO",
            "761337610411265304^^^SPID&2.16.756.5.30.1.127.3.10.3&ISO",
            "761337610411265888^^^SPID&2.16.756.5.30.1.127.3.10.3&ISO",
            "761337610411265222^^^SPID&2.16.756.5.30.1.127.3.10.3&ISO",
            "761337610435200998^^^&2.16.756.5.30.1.127.3.10.3&ISO",
            "761337610435209810^^^&2.16.756.5.30.1.127.3.10.3&ISO",
            "761337610436974489^^^&2.16.756.5.30.1.127.3.10.3&ISO",
            "761337610430891416^^^&2.16.756.5.30.1.127.3.10.3&ISO",
            "761337610423590456^^^&2.16.756.5.30.1.127.3.10.3&ISO",
            "761337610455909127^^^&2.16.756.5.30.1.127.3.10.3&ISO",
            "761337610445502987^^^&2.16.756.5.30.1.127.3.10.3&ISO",
            "761337610448027647^^^&2.16.756.5.30.1.127.3.10.3&ISO",
            "761337610469261945^^^&2.16.756.5.30.1.127.3.10.3&ISO",
            "761337610510635763^^^&2.16.756.5.30.1.127.3.10.3&ISO",
            "761337610433933946^^^&2.16.756.5.30.1.127.3.10.3&ISO",
            "761337610411265456^^^&2.16.756.5.30.1.127.3.10.3&ISO",
            "761337610411265789^^^&2.16.756.5.30.1.127.3.10.3&ISO",
            "761337610411265777^^^&2.16.756.5.30.1.127.3.10.3&ISO",
            "761337610411265304^^^&2.16.756.5.30.1.127.3.10.3&ISO",
            "761337610411265888^^^&2.16.756.5.30.1.127.3.10.3&ISO",
            "761337610411265222^^^&2.16.756.5.30.1.127.3.10.3&ISO"]

}

String[] getGlnList() {
    return ["7601000050717",
            "7601002033572",
            "7601002469191",
            "7601002467373",
            "7601002468282",
            "7601002466565"]
}

String[] getGlnAssList() {
    return ["7601002467158",
            "7601002466812",
            "7601002462586"]
}

String[] getGlnTcuList() {
    return ["7601002461111"]
}

String getGroupIdFromHcpGln(String hcp_gln) {
    if (hcp_gln.equals("7601000050717")) {
        return "urn:oid:2.16.10.89.211";
    } else if (hcp_gln.equals("7601002033572")) {
        return "urn:oid:2.16.10.89.211";
    } else if (hcp_gln.equals("7601002469191")) {
        return "urn:oid:2.16.10.89.211";
    } else if (hcp_gln.equals("7601002467373")) {
        return "urn:oid:2.16.10.89.211"
    } else if (hcp_gln.equals("7601002468282")) {
        return "urn:oid:2.16.10.89.211"
    } else if (hcp_gln.equals("7601002466565")) {
        return "urn:oid:2.16.10.89.211"
    } else {
        return null;
    }
}

String getOrganizationNameFromId(String id) {
    if (id.equals("urn:oid:2.16.10.89.211")) {
        return "Spital Y";
    } else {
        return null;
    }
}

String getNameFromGln(String hcp_gln) {
    //HCP
    if (hcp_gln.equals("7601000050717")) {
        return "Marc Loris Agpar";
    } else if (hcp_gln.equals("7601002033572")) {
        return "Rose Spieler";
    } else if (hcp_gln.equals("7601002469191")) {
        return "Ann Andrews";
    } else if (hcp_gln.equals("7601002467373")) {
        return "Richard Reynolds"
    } else if (hcp_gln.equals("7601002468282")) {
        return "Camille Bouchard";
    } else if (hcp_gln.equals("7601002466565")) {
        return "Matthew Marston";
        //ASS
    } else if (hcp_gln.equals("7601002467158")) {
        return "Lara Tièche";
    } else if (hcp_gln.equals("7601002466812")) {
        return "Cihan Berger";
    } else if (hcp_gln.equals("7601002462586")) {
        return "Taulant Ravasi";
    } else {
        return null;
    }
}

String getGlnFromNameId(String hcp_id_subject) {
    if (hcp_id_subject.equals("magpar")) {
        return "7601000050717";
    } else if (hcp_id_subject.equals("rspieler")) {
        return "7601002033572";
    } else if (hcp_id_subject.equals("aandrews")) {
        return "7601002469191";
    } else if (hcp_id_subject.equals("rreynolds")) {
        return "7601002467373"
    } else if (hcp_id_subject.equals("cbouchard")) {
        return "7601002468282";
    } else if (hcp_id_subject.equals("mmarston")) {
        return "7601002466565";
    } else {
        return null;
    }
}

String getPatientIdFromNameId(String pat_id_subject) {

    if (pat_id_subject.equals("wwalters")) {
        return "761337610435200998";
    }
    if (pat_id_subject.equals("bovie")) {
        return "761337610435209810";
    }
    if (pat_id_subject.equals("lavdic")) {
        return "761337610436974489";
    }
    if (pat_id_subject.equals("maschwanden")) {
        return "761337610430891416";
    }
    if (pat_id_subject.equals("gantonyova")) {
        return "761337610423590456";
    }
    if (pat_id_subject.equals("aamrein")) {
        return "761337610455909127";
    }
    if (pat_id_subject.equals("sebibi")) {
        return "761337610445502987";
    }
    if (pat_id_subject.equals("negger")) {
        return "761337610448027647";
    }
    if (pat_id_subject.equals("aerne")) {
        return "761337610469261945";
    }
    if (pat_id_subject.equals("dozkanturk")) {
        return "761337610510635763";
    }
    if (pat_id_subject.equals("remery")) {
        return "761337610433933946";
    }
    if (pat_id_subject.equals("nwittwerchristen")) {
        return "761337610411265304";
    }
    if (pat_id_subject.equals("rregez")) {
        return "761337610411265777";
    }
    if (pat_id_subject.equals("ftenaglia")) {
        return "761337610411265456";
    }
    return null;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////