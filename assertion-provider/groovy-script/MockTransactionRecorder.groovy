import org.apache.commons.lang.StringUtils;
import net.ihe.gazelle.wstester.mockrecord.Message
import net.ihe.gazelle.wstester.mockrecord.MessageRecorder
import net.ihe.gazelle.wstester.mockrecord.EStandard
import net.ihe.gazelle.wstester.mockrecord.MessageException
import java.nio.charset.StandardCharsets


def final DEFAULT_JDBC_URL = "jdbc:postgresql://localhost:5432/gazelle-webservice-tester"
def final DEFAULT_DB_USER = "gazelle"
def final DEFAULT_DB_USER_PASSWORD = "gazelle"
/////////////////////////////////////
/////////////////////////////////////
def final INITIATOR_ACTOR = "X-SERVICE_USER"
def final RESPONDER_ACTOR = "X-ASSERTION_PROVIDER"
def final DOMAIN = "EPR"
def final TRANSACTION = "CH:XUA"
def final SIMULATOR_IP = "AssertionProviderSimulator"
def final STANDARD_USED = EStandard.OTHER
/////////////////////////////////////
/////////////////////////////////////

def jdbcUrl = getParameter("jdbcURL", DEFAULT_JDBC_URL)
def dbUser = getParameter("dbUser", DEFAULT_DB_USER)
def dbUserPassword = getParameter("dbUserPassword", DEFAULT_DB_USER_PASSWORD)

def request = mockRequest.requestContent;
def response = mockResponse.responseContent;
def sender_ip = mockRequest.getHttpRequest().getRemoteAddr()
def requestType = getMessageType(request)
def responseType = getMessageType(response)

response = expandResponseProperties(response)

try {
    log.info "Record transaction messages on " + jdbcUrl + " as " + dbUser + "..."
    MessageRecorder messageRecorder = new MessageRecorder(jdbcUrl, dbUser, dbUserPassword)
    Message requestMessage = new Message(sender_ip, sender_ip, requestType, INITIATOR_ACTOR, request.getBytes(StandardCharsets.UTF_8))
    Message responseMessage = new Message(SIMULATOR_IP, SIMULATOR_IP, responseType, RESPONDER_ACTOR, response.getBytes(StandardCharsets.UTF_8))
    messageRecorder.record(STANDARD_USED, TRANSACTION, DOMAIN, RESPONDER_ACTOR, requestMessage, responseMessage)
    log.info "Record successful"
} catch(Exception e) {
    log.warn("Unable to record transaction messages", e)
}



/////////////////////////////////////////////////////////////////////////////////////

def getParameter(def propertyName, def defaultValue) {
    def property = context.mockService.project.getPropertyValue(propertyName)
    return property != null && !property.isEmpty() ? property : defaultValue
}

def getMessageType(def message) {
    return new XmlSlurper().parseText(message).Body.'*'[0].name()
}

def expandResponseProperties(def response) {
    int countProperties = StringUtils.countMatches(response.toString(), '${')
    for (int i = 1; i <= countProperties; i++) {
        def propertyTag = response.substring(response.indexOf('${'), response.indexOf('}') + 1)
        def propertyName = propertyTag.substring(propertyTag.lastIndexOf('{') + 1, propertyTag.indexOf('}'))

        if (requestContext.getProperty(propertyName) != null) {
            response = response.replace(propertyTag, requestContext.getProperty(propertyName))
        } else {
            response = response.replace(propertyTag, "")
        }
    }
    return response
}